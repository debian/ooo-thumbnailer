#!/usr/bin/env python
# 2009 David D Lowe
# To the extent possible under law, David D. Lowe has waived all copyright and related or neighboring rights to this file.
# License: http://creativecommons.org/publicdomain/zero/1.0/

from distutils.core import setup

def main():
        
    setup( name="ooo-thumbnailer", # this should start with a lowercase letter 
        #so that it can be used as a debian package name later on
    version="0.2", # string, version of your program, not python version
    description="OpenOffice.org thumbnailer for OpenOffice.org documents, spreadsheets, presentations and drawings.", # short
    author="David D Lowe",
    author_email="DavidDLowe.flimm@gmail.com",
    url="", # home page for end-users
    license="GPL v2",
    data_files=[('/usr/share/gconf/schemas', ['ooo-thumbnailer.schemas'])],
    scripts=["ooo-thumbnailer"], # the script that should be run and installed in /usr/bin
    classifiers=["Development Status :: 1 - Planning", "License :: OSI Approved :: BSD License", "Operating System :: POSIX :: Linux"],
        # a bunch of optional tags, a list of classifiers can be found at http://pypi.python.org/pypi?:action=list_classifiers
    long_description="""ooo-thumbnailer is a lightweight OpenOffice.org document thumbnailer that can be used by file
managers to create thumbnails for your documents, spreadsheets, presentations and drawings.""")

if __name__ == "__main__":
    main()
